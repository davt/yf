
# Standard Library Imports
import importlib

# 3rd Party Library Imports
import numpy as np

from pandas import DataFrame, Series

import matplotlib
matplotlib.use('TkAgg')
import matplotlib.pyplot as plt

from scipy import signal

import yfinance as yf


def plot_ticker(ticker, period, filter_time):
    data = yf.download(
        period=period,
        tickers=ticker,
    )

    data.Close.plot()
    data.Close.rolling(window=filter_time, center=False).mean().plot()
    data.High.plot()
    data.Low.plot()
    plt.title(ticker)
    plt.grid()
    plt.show()


def main():
    with open('tickers') as tickers:
        for ticker in tickers:
            plot_ticker(ticker, '5y', 50)


def smooth(data, n=14):
    output, _ = signal.lfilter([1], [n, 1 - n], data, zi=[data[0]])
    return output


def investment_strategy_awt(data):
    """
    When to buy filters
    1.  ADX score >25
    2.  DI+ score > DI- score
    3.  EMA20 > EMA50 > EMA200
    4.  All EMA in upward direction (not sure this situation would exist,
        so may require the 20 EMA to be starting to slope down?)
    5.  a daily low lower than the previous 2 weekly lows.

    When to buy set up
    1. All filters positive and price rises above the previous weeks high.

    Risk Management
    1. Risk no more than 1% of our capital on every trade.
    2. Set the stop loss 1.5 times the range of the previous days range.

    When to sell trade.
    1 When any of the buy filters 1-4 no longer met.
    """
    adx = data.ADX.to_numpy()
    pos_di = data.PosDI.to_numpy()
    neg_di = data.NegDI.to_numpy()
    ema_20 = data.EMA20.to_numpy()
    delta_ema_20 = np.insert(ema_20[1:] - ema_20[:-1], 0, 0)
    # delta_ema_20 = np.insert(delta_ema_20, 0, 0)
    ema_50 = data.EMA50.to_numpy()
    delta_ema_50 = np.insert(ema_50[1:] - ema_50[:-1], 0, 0)
    # delta_ema_50 = np.insert(delta_ema_50, 0, 0)
    ema_200 = data.EMA200.to_numpy()
    delta_ema_200 = np.insert(ema_200[1:] - ema_200[:-1], 0, 0)
    # delta_ema_200 = np.insert(delta_ema_200, 0, 0)

    prev_lows = []
    lows = [0]
    for low in data.Low:
        prev_lows.append(min(lows[-10:]))
        lows.append(low)
    prev_lows = np.array(prev_lows)
    lows = np.array(lows[1:])

    buy_filter = (
        (adx > 25)
        & (pos_di > neg_di)
        & (ema_20 > ema_50)
        & (ema_50 > ema_200)
        # & (delta_ema_20 > 0)
        & (delta_ema_50 > 0)
        & (delta_ema_200 > 0)
        & (lows < prev_lows)
    )

    _buy_flag = False
    buy = []
    sell = []
    for buy_flag in buy_filter:
        buy.append(not _buy_flag and buy_flag)
        sell.append(_buy_flag and not buy_flag)
        _buy_flag = buy_flag

    print(np.size(buy_filter), np.size(delta_ema_20))

    data['Buy'] = Series(buy, data.index)
    data['Sell'] = Series(sell, data.index)


def investment_strategy_dwt(data):
    short_avg = data.EMA50.to_numpy()
    long_avg = data.EMA200.to_numpy()

    buy_signal = short_avg > long_avg
    sell_signal = short_avg < long_avg

    _buy_flag = False
    buy = []
    for buy_flag in buy_signal:
        buy.append(not _buy_flag and buy_flag)
        _buy_flag = buy_flag

    _sell_flag = False
    sell = []
    for sell_flag in sell_signal:
        sell.append(not _sell_flag and sell_flag)
        _sell_flag = sell_flag

    data['Buy'] = Series(buy, data.index)
    data['Sell'] = Series(sell, data.index)



def run_investments(data, starting_capital, risk):
    holding = 0
    capital = starting_capital
    worth = []
    for i in range(len(data)):
        if data.Buy[i]:
            investment = risk * capital
            holding = investment / data.Close[i]
            capital -= investment
            print(f'BUYING {data.index[i]} {capital} {data.Close[i]} {investment} {holding}')
        if data.Sell[i]:
            capital += holding * data.Close[i]
            holding = 0
            print(f'SELLING {data.index[i]} {capital} {data.Close[i]} 0 0')

        worth.append(capital + holding * data.Close[i])
    data['Worth'] = Series(worth, data.index)


def add_yesterdays_values(data):
    for value in 'High', 'Low', 'Open', 'Close':
        data[f'Yesterday{value}'] = Series(data[value].to_numpy()[:-1], data.index[1:])


def calculate_metrics(data):
    # UpMove = today's high − yesterday's high
    # DownMove = yesterday's low − today's low
    add_yesterdays_values(data)
    data['UpMove'] = data.High - data.YesterdayHigh
    data['DownMove'] = data.YesterdayLow - data.Low

    # if UpMove > DownMove and UpMove > 0, then +DM = UpMove, else +DM = 0
    data['PosDM'] = Series(0, data.index)
    data.PosDM.loc[(data.UpMove > data.DownMove) & (data.UpMove > 0)] = data.UpMove

    # if DownMove > UpMove and DownMove > 0, then -DM = DownMove, else -DM = 0
    data['NegDM'] = Series(0, data.index)
    data.NegDM.loc[(data.DownMove > data.UpMove) & (data.DownMove > 0)] = data.DownMove

    data['TrueRange'] = DataFrame([
        data.High - data.Low,
        (data.High - data.YesterdayClose).abs(),
        (data.Low - data.YesterdayClose).abs(),
    ]).max()
    data['AverageTrueRange'] = Series(smooth(data.TrueRange.to_numpy()), data.index)

    # +DI = 100 times the smoothed moving average of (+DM) divided by average true range
    data['PosDMSmoothed'] = Series(smooth(data.PosDM.to_numpy()), data.index)
    data['PosDI'] = 100 * data.PosDMSmoothed / data.AverageTrueRange

    # -DI = 100 times the smoothed moving average of (-DM) divided by average true range
    data['NegDMSmoothed'] = Series(smooth(data.NegDM.to_numpy()), data.index)
    data['NegDI'] = 100 * data.NegDMSmoothed / data.AverageTrueRange

    data['AdxRaw'] = 100 * (data.PosDI - data.NegDI).abs() / (data.PosDI + data.NegDI)
    data.fillna(0.0, inplace=True)
    data['ADX'] = Series(smooth(data.AdxRaw.to_numpy()), data.index)

    data['EMA20'] = Series(smooth(data.Close.to_numpy(), 20), data.index)
    data['EMA50'] = Series(smooth(data.Close.to_numpy(), 50), data.index)
    data['EMA200'] = Series(smooth(data.Close.to_numpy(), 200), data.index)


def test():
    data = yf.download(period='30y', tickers='^FTSE')

    calculate_metrics(data)
    investment_strategy_dwt(data)
    run_investments(data, 10000, 0.1)

    plt.subplot(221)
    data.Close.plot(color='blue')
    data.EMA20.plot()
    data.EMA50.plot()
    data.EMA200.plot()
    plt.grid()

    plt.subplot(222)
    data.ADX.plot(color='orange')
    data.PosDI.plot(color='green')
    data.NegDI.plot(color='red')
    plt.grid()

    plt.subplot(223)
    buy_signal = 0 * np.ones(np.size(data.Buy))
    buy_signal[data.Buy] = 0.4
    buy_signal = Series(buy_signal, data.index)
    buy_signal.plot(color='green')
    sell_signal = 0.5 * np.ones(np.size(data.Sell))
    sell_signal[data.Sell] = 0.9
    sell_signal = Series(sell_signal, data.index)
    sell_signal.plot(color='red')
    plt.grid()

    plt.subplot(224)
    data.Worth.plot(color='blue')
    plt.grid()

    plt.show()


if __name__ == '__main__':
    test()

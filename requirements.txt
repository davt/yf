yfinance==0.1.45
lxml==4.4.1
html5lib==1.0.1
beautifulsoup4==4.8.0
matplotlib==3.1.1
scipy==1.3.2
numpy==1.17.2
pandas==0.25.1

pytest==5.4.1
pylint==2.4.4
pylint-mccabe==0.1.3
mypy==0.770
